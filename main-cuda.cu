#include "sha256.cuh"
#include <stdio.h>
#include <bits/stdc++.h>
#include <assert.h>
#include <assert.h>

using namespace std;
#define ull unsigned long long int

// Currently statically defining the maximum possible for most of the GPUs. Need to change this to read from device props later.
#define BLOCK_SIZE 512
#define NUM_BLOCKS 4096
// Setting the maximum search space to the below variable.
#define MAX_VAL INT_MAX
// Time before which we need to decrease the complexity
#define  MAX_DURATION 30000
// Number of blocks to mine
#define NO_BLOCKS 10

// Global Device nonce value, if a value of anything less than ULLONG_MAX is set in this, we assume nonce was found.
__device__ ull g_d_nonce;
__device__ BYTE
g_res_hash_string[2*SHA256_BLOCK_SIZE];

// Function to strip last chars and append the digits of the given number 'n'
__device__ void find_block_string(ull n, BYTE *output) {
    ull i = 0;
    if (n == 0) {
        output[2 * SHA256_BLOCK_SIZE - 1] = '0';
    }
    while (n) {
        output[2 * SHA256_BLOCK_SIZE - i - 1] = ('0' + n % 10);
        n /= 10;
        i++;
    }
    output[2 * SHA256_BLOCK_SIZE] = '\0';
}

// Function to convert decimal to hexadecimal
// Can be optimized: Currently we are first converting to uppercase and then to lowercase.
__device__ void decimalToHexa(BYTE * d, BYTE * res) {
    int i, n, j = 0, temp = 0;
    // Explicitly set last character as null to avoid any garbage.
    d[SHA256_BLOCK_SIZE * 2] = '\0';
    // Convert from hex to string representation
    for (i = 0; i < SHA256_BLOCK_SIZE; i++) {
        n = (int) d[i];
        temp = n % 16;
        res[j + 1] = temp < 10 ? temp + 48 : temp + 55;
        n = n / 16;
        temp = n % 16;
        res[j] = temp < 10 ? temp + 48 : temp + 55;
        j += 2;
    }

    // Convert to lowercase
    for (i = 0; i < SHA256_BLOCK_SIZE * 2; i++) {
        if ((res[i] >= 'A') && (res[i] <= 'Z')) {
            res[i] = res[i] - ('A' - 'a');
        }
    }
    // Explicitly set last character as null to avoid any garbage.
    res[SHA256_BLOCK_SIZE * 2] = '\0';
}

// Function to compare the char-by-char string representation of the numbers.
// This could be also done by comparing the hex representations, but not sure if we could give the target in hex.
__device__ int string_compare(BYTE * a, BYTE * b) {
    for (int i = 0; i < 2 * SHA256_BLOCK_SIZE; i++) {
        if (a[i] > b[i]) {
            return 1;
        } else if (a[i] < b[i]) {
            return 0;
        }
    }
    return 0;
}

// Redundant: GPU function to compute the next target hash.
__device__ void compute_target_hash(BYTE s[], int iter, int duration) {
    s[2 * SHA256_BLOCK_SIZE] = '\0';
    BYTE res[2 * SHA256_BLOCK_SIZE] = {0};
    memcpy(res, s, 2 * SHA256_BLOCK_SIZE);
    if (duration < MAX_DURATION) {
        res[0] = '0';
        for (int i = 1; i < 2 * SHA256_BLOCK_SIZE; i++) {
            res[i] = s[i - 1];
        }
    } else {
        for (int i = 0; i < 2 * SHA256_BLOCK_SIZE - 1; i++) {
            res[i] = s[i + 1];
        }
    }
    memcpy(s, res, 2 * SHA256_BLOCK_SIZE);
}

// Kernel function to find the required nonce.
__global__ void gpu_compute(BYTE * block_string, BYTE * target_hash) {
    ull i, index = threadIdx.x + blockIdx.x * blockDim.x;
    ull stride = blockDim.x * gridDim.x;
    BYTE local_block_string[2 * SHA256_BLOCK_SIZE] = {0};
    BYTE intermediate_res[SHA256_BLOCK_SIZE] = {0};
    BYTE string_output[2 * SHA256_BLOCK_SIZE] = {0};
    SHA256_CTX ctx1;
    memcpy(local_block_string, block_string, 2 * SHA256_BLOCK_SIZE);
    for (i = index; i < MAX_VAL && g_d_nonce == ULLONG_MAX; i += stride) {
        find_block_string(i, local_block_string);
        sha256_init(&ctx1);
        sha256_update(&ctx1, local_block_string, 2 * SHA256_BLOCK_SIZE);
        sha256_final(&ctx1, intermediate_res);
        decimalToHexa(intermediate_res, string_output);
        sha256_init(&ctx1);
        sha256_init(&ctx1);
        sha256_update(&ctx1, string_output, 2 * SHA256_BLOCK_SIZE);
        sha256_final(&ctx1, (BYTE *) intermediate_res);
        decimalToHexa(intermediate_res, string_output);
        if (string_compare(target_hash, string_output)) {
//            printf("Found the satisfying hash value as::: %s\n", string_output);
            g_d_nonce = i;
            memcpy(g_res_hash_string, string_output, 2 * SHA256_BLOCK_SIZE);
            break;
        }
    }
}


void cpu_compute_target_hash(BYTE s[], int iter, int duration, bool block_nonce_found) {
    s[2 * SHA256_BLOCK_SIZE] = '\0';
    char res[2 * SHA256_BLOCK_SIZE] = {0};
    memcpy(res, s, 2 * SHA256_BLOCK_SIZE);
    int in, hex_size = 8;
    if ((duration < MAX_DURATION) && block_nonce_found) {
        for (int i = 0; i < 2 * SHA256_BLOCK_SIZE; i += hex_size) {
            char str[hex_size];
            in = 0;
            for (int j = i; j < i + hex_size; j++) {
                str[in] = s[j];
                in++;
            }
            unsigned long ucByte = strtoul(str, NULL, 16);
            sprintf(res + i, "%08x", (ucByte / 2));
        }
    } else {
        for (int i = 0; i < 2 * SHA256_BLOCK_SIZE; i += hex_size) {
            char str[hex_size];
            in = 0;
            for (int j = i; j < i + hex_size; j++) {
                str[in] = s[j];
                in++;
            }
            unsigned long ucByte = strtoul(str, NULL, 16);
            sprintf(res + i, "%08x", (ucByte * 2));
        }
    }
    memcpy(s, res, 2 * SHA256_BLOCK_SIZE);
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " initialHash targetHash" << std::endl;
        return 1;
    } else {
        cudaSetDevice(3);
        cudaFree(0);
        int number_of_blocks = NO_BLOCKS;
        int block_iter = 0;
        checkCudaErrors(cudaMemcpyToSymbol(dev_k, host_k, sizeof(host_k), 0, cudaMemcpyHostToDevice));
        cudaError err;
        cout << "BPG: " << NUM_BLOCKS << ", TPB: " << BLOCK_SIZE << endl;

        BYTE initial_block_hash[2*SHA256_BLOCK_SIZE] = {0};
        BYTE target_hash[2*SHA256_BLOCK_SIZE] = {0};
        for (int i=0;i<2*SHA256_BLOCK_SIZE; i++) {
            initial_block_hash[i] = argv[1][i];
            target_hash[i] = argv[2][i];
        }

        //    BYTE target_hash[] = "0088888888888882892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
        //    BYTE initial_block_hash[] = "ac9a17968e67b435be39ea10aac6149c678c3d645e9d0c932f3e73ee8cc935ea";
        // BYTE target_hash[] = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
        // BYTE initial_block_hash[] = "0aca36d7d8e3bd46e6bab5bf3a47230e91e100ccd241c169e9d375f5b2a28f82";
        ull nonce_values[number_of_blocks];
        BYTE resulting_hash[number_of_blocks][2 * SHA256_BLOCK_SIZE];
        BYTE target_hash_array[number_of_blocks][2 * SHA256_BLOCK_SIZE];
        BYTE first_result_hash[2 * SHA256_BLOCK_SIZE];
        double duration_values[number_of_blocks];
        int size = 0;
        BYTE * d_data;
        BYTE * d_target_hash;
        cudaMalloc((void **) &d_data, 2 * SHA256_BLOCK_SIZE);
        cudaMalloc((void **) &d_target_hash, 2 * SHA256_BLOCK_SIZE);
        auto t1_overall = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::milliseconds>(t1_overall - t1_overall).count();
        bool block_nonce_found = true;
        while (block_iter < number_of_blocks) {
            auto t1 = chrono::high_resolution_clock::now();
            size = 0;
            if (block_iter != 0)
                cpu_compute_target_hash(target_hash, block_iter, duration, block_nonce_found);
            target_hash[2 * SHA256_BLOCK_SIZE] = '\0';
            memcpy(target_hash_array[block_iter], target_hash, 2 * SHA256_BLOCK_SIZE);
            BYTE * s = target_hash;
            while (*s++) {
                size++;
            }
            assert(size == 2 * SHA256_BLOCK_SIZE);

            cudaMemcpy(d_data, initial_block_hash, size * sizeof(BYTE), cudaMemcpyHostToDevice);
            cudaMemcpy(d_target_hash, target_hash, size * sizeof(BYTE), cudaMemcpyHostToDevice);
            ull h_nonce = ULLONG_MAX;
            cudaMemcpyToSymbol(g_d_nonce, &h_nonce, sizeof(ull));
            err = cudaGetLastError();
            if (cudaSuccess != err) {
                printf("\n\nError: !\n");
                printf("%s", cudaGetErrorString(err));
            }
            gpu_compute << < NUM_BLOCKS, BLOCK_SIZE >> > (d_data, d_target_hash);
            cudaDeviceSynchronize();
            cudaMemcpyFromSymbol(&h_nonce, g_d_nonce, sizeof(ull));
            cudaMemcpyFromSymbol(resulting_hash[block_iter], g_res_hash_string, 2 * SHA256_BLOCK_SIZE);
            // Currently the last block_iter somehow manages to erase the first block. Hence copying the first one in a different variable.
            if (block_iter == 0)
                memcpy(first_result_hash, resulting_hash[block_iter], 2 * SHA256_BLOCK_SIZE);
            memcpy(&nonce_values[block_iter], &h_nonce, sizeof(ull));
            if (nonce_values[block_iter] == ULLONG_MAX) {
                block_nonce_found = false;
            } else {
                block_nonce_found = true;
            }
            err = cudaGetLastError();
            if (cudaSuccess != err) {
                printf("\n\nError: !\n");
                printf("%s", cudaGetErrorString(err));
            }
            auto t2 = chrono::high_resolution_clock::now();
            duration = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();
            duration_values[block_iter] = duration;
            block_iter++;
        }
        auto t2_overall = chrono::high_resolution_clock::now();
        auto duration_overall = chrono::duration_cast<chrono::milliseconds>(t2_overall - t1_overall).count();
        for (block_iter = 0; block_iter < number_of_blocks; block_iter++) {
            memcpy(target_hash, target_hash_array[block_iter], 2 * SHA256_BLOCK_SIZE);
            if (block_iter == 0)
                memcpy(initial_block_hash, first_result_hash, 2 * SHA256_BLOCK_SIZE);
            else
                memcpy(initial_block_hash, resulting_hash[block_iter], 2 * SHA256_BLOCK_SIZE);
            if (nonce_values[block_iter] == ULLONG_MAX) {
                printf("\n Block: %d, target_hash: %s, * NO NONCE * duration(ms): %lf", block_iter,
                       target_hash, duration_values[block_iter]);
            } else {
                printf("\n Block: %d, target_hash: %s, Nonce: %llu,  res hash: %s, duration(ms): %lf", block_iter,
                       target_hash, nonce_values[block_iter],
                       initial_block_hash,
                       duration_values[block_iter]
                );
            }
        }
        printf("\nCompleted in %lu ms!!\n", duration_overall);
        return 0;
    }
}




// Computes the next target, if duration was small, append 0, else remove leading zero
//BYTE* compute_target_hash(BYTE* prev_target, int iter, int duration) {
//    int i, size = 2*SHA256_BLOCK_SIZE;
//    BYTE new_target[2*SHA256_BLOCK_SIZE] = {0};
//    memcpy(new_target, prev_target, 2*SHA256_BLOCK_SIZE);
//    if (duration < MAX_DURATION) {
//        i=0;
//        while(new_target[i] == '0'){
//            i++;
//        }
//        char to_insert = '0';
//        while(i<size) {
//            new_target[i] = new_target[i-1];
//            i--;
//        }
//        new_target[i-1] = '0'
//    } else {
//        i=0;
//        while(new_target[i] == '0') {
//            i++;
//        }
//        new_target[i-1] = '0'
//    }
//    return new_target;
//}
