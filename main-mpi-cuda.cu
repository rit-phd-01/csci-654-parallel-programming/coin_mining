#include <bits/stdc++.h>
#include <climits>
#include <mpi.h>
#include "sha256.h"
// #include "coin_mining_gpu.cu"
#define ull unsigned long long
#define MAX_VAL INT_MAX
#define  MAX_DURATION 30000
#define SHA256_BLOCK_SIZE 32

using namespace std;

int numDigits(int32_t x) {
    if (x == INT_MIN) return 10 + 1;
    if (x < 0) return numDigits(-x) + 1;

    if (x >= 10000) {
        if (x >= 10000000) {
            if (x >= 100000000) {
                if (x >= 1000000000)
                    return 10;
                return 9;
            }
            return 8;
        }
        if (x >= 100000) {
            if (x >= 1000000)
                return 7;
            return 6;
        }
        return 5;
    }
    if (x >= 100) {
        if (x >= 1000)
            return 4;
        return 3;
    }
    if (x >= 10)
        return 2;
    return 1;
}

string find_block_string(ull n, string input) {
    ull i = 0;
    string output = input;
    if (n == 0) {
        output[2 * SHA256_BLOCK_SIZE - 1] = '0';
    }
    while (n) {
        output[2 * SHA256_BLOCK_SIZE - i - 1] = ('0' + n % 10);
        n /= 10;
        i++;
    }
    output[2 * SHA256_BLOCK_SIZE] = '\0';
    return output;
}

unsigned long long
proof_of_work(string block_hash, string target_hash, ull start_index, ull end_index, int world_rank, ull *res,
              MPI_Win *window) {
    unsigned long long int i;
    // string test_string, test_hash;
    string test_hash;
    // char test_string[2 * SHA256_BLOCK_SIZE] = {0};
    string test_string;
//    cout << world_rank << " says target_hash:: " << target_hash << " start: " << start_index << ", end_index: "
//         << end_index << endl;
    for (i = start_index; i < end_index; i++) {
        test_string = find_block_string(i,
                                        block_hash); //block_hash.substr(0, block_hash.size() - numDigits(i)) + to_string(i);
//        if (world_rank == 0 && i < 10) {
//            cout << "\n Got block_string as:: " << test_string;
//        }
        test_hash = sha256(sha256(test_string));
        // As soon as nonce is found, put it on the RMA space of 0th node.
        if (test_hash.compare(target_hash) < 0) {
            cout << "\nHash found is:: " << test_hash << " test string was:: " << test_string << " nonce:: "
                 << i << endl;
            *res = i;
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, *window);
            MPI_Put(res, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, 1, MPI_UNSIGNED_LONG_LONG, *window);
            MPI_Win_unlock(0, *window);

            return i;
        }
        // Check on every 100th iteration of someone found the nonce.
        if (i % 100 == 0) {
            MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, *window);
            MPI_Get(res, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, 1, MPI_UNSIGNED_LONG_LONG, *window);
            MPI_Win_unlock(0, *window);
            if (*res != ULLONG_MAX) {
                break;
            }
        }
    }
    return ULLONG_MAX;
}

string generate_target(string s, bool half) {
    string res = s;
    if (half) {
        for (int i = 0; i < s.length(); i++) {
            if (s[i] < 58 && s[i] > 47) {
                res[i] = (s[i] - '0') / 2 + '0';
            } else {
                res[i] = (s[i] - 'a' + 10) / 2 + '0';
            }
        }
    } else {
        for (int i = 0; i < s.length(); i++) {
            if (s[i] < 58 && s[i] > 47) {
                res[i] = (s[i] - '0') * 2 + '0';
            } else {
                if (s[i] == 'f' && s[i - 1] < 'f' && i > 0) {
                    s[i - 1] = s[i - 1] + 1;
                }
                res[i] = (((s[i] - 'a' + 10) * 2) % 16) + '0';
                if (res[i] > 57) {
                    res[i] = res[i] - '9' + 'a';
                }
            }
        }
    }
    return res;
}

// Computes the next target, if duration was small, append 0, else remove leading zero
string compute_target_hash(string prev_target, int iter, int duration) {
    string new_target;
    int size = prev_target.size();
    if (duration < MAX_DURATION) {
        new_target = to_string(0) + prev_target.substr(0, size - 1); // generate_target(prev_target, false);
    } else {
        new_target = prev_target.substr(1, size) + to_string(rand() % 10);
    }
    return new_target;
}

int compute_leading_zeros(string target) {
    int i = 0;
    while (target[i] == '0') {
        i++;
    }
    return i;
}

int main() {
    auto t1_full = chrono::high_resolution_clock::now();
    int number_of_blocks = 1;
    string block_hash = sha256("abc"); // sha256("The quick brown fox jumps over the lazy dog!!!!!!");
    string target_hash = "000ab00090ba1a5e38c216ee6790fc1dbc18e8a447b61ec2133ae6cccc000000";
    MPI_Init(NULL, NULL);
    MPI_Win win;
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_rank == 0) {
        cout << "Starting on " << world_size << " threads!!" << endl;
    }

    int is_cuda_device_present = 0;
    int nDevices = 0;
    if (world_rank == 0) {
        MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &nDevices);
        // Create a window to check the number of devices.
        MPI_Win_allocate(sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &is_cuda_device_present, &win);
        // Initally set the nDevices as 0; Only one node can set it, rest will anyhow see its value, no need for everyone to write.
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, win);
        MPI_Put(&nDevices, 1, MPI_INT, 0, 0, 1, MPI_INT, win);
        MPI_Win_unlock(0, win);

    } else {
        // Worker processes do not expose memory in the window
        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    }
    auto t2_full = chrono::high_resolution_clock::now();
    auto duration_full = chrono::duration_cast<chrono::milliseconds>(t2_full - t1_full).count();
    MPI_Barrier(MPI_COMM_WORLD);
    if(world_rank == 0)
	cout << " To create a shared memory: " << duration_full << endl;
    t1_full = chrono::high_resolution_clock::now();
    cudaError_t err = cudaGetDeviceCount(&nDevices);
    // Error 35 is just cuda driver compatiblity issue.
    if (err != cudaSuccess && err != 35)
        printf("Error getting devices on thread %d as : %d: %s\n", world_rank, err, cudaGetErrorString(err));
    if (nDevices > 0) {
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, win);
        MPI_Put(&nDevices, 1, MPI_INT, 0, 0, 1, MPI_INT, win);
        MPI_Win_unlock(0, win);
    }
    t2_full = chrono::high_resolution_clock::now();
    duration_full = chrono::duration_cast<chrono::milliseconds>(t2_full - t1_full).count();
    MPI_Barrier(MPI_COMM_WORLD);
    t1_full = chrono::high_resolution_clock::now();
    if(world_rank == 0)
        cout << " To set is_cuda_device_present: " << duration_full << endl;
    MPI_Win_lock(MPI_LOCK_SHARED, 0, 0, win);
    MPI_Get(&is_cuda_device_present, 1, MPI_INT, 0, 0, 1, MPI_INT, win);
    MPI_Win_unlock(0, win);
    t2_full = chrono::high_resolution_clock::now();
    duration_full = chrono::duration_cast<chrono::milliseconds>(t2_full - t1_full).count();
    if(world_rank == 0)
        cout << " To get is_cuda_device_present:: " << duration_full << endl;

    if (!is_cuda_device_present) {
        if(world_rank == 0) {
            cout << "No CUDA devices found, going ahead with MPI version!!" << endl;
        }
        ull chunk_size = ceil((MAX_VAL) / world_size);
        ull s_index = (world_rank) * chunk_size;
        ull e_index = (world_rank + 1) * chunk_size - 1;
        int block_iter = 0;
        double hash_rate = 0;
        auto t1_overall = chrono::high_resolution_clock::now();
        while (block_iter++ < number_of_blocks) {
            auto t1 = chrono::high_resolution_clock::now();
            ull result;
            MPI_Win_allocate(sizeof(ull), sizeof(ull), MPI_INFO_NULL, MPI_COMM_WORLD, &result, &win);
            result = ULLONG_MAX;
            if (world_rank == 0) {
                MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, win);
                MPI_Put(&result, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, 1, MPI_UNSIGNED_LONG_LONG, win);
                MPI_Win_unlock(0, win);
            }
            MPI_Barrier(MPI_COMM_WORLD);

            if (world_rank == 0) {
                cout << "Block: " << block_iter;
            }
            auto t1_internal = chrono::high_resolution_clock::now();
            ull local_result = proof_of_work(block_hash, target_hash, s_index, e_index, world_rank, &result, &win);
            auto t2_internal = chrono::high_resolution_clock::now();
            auto duration_internal = chrono::duration_cast<chrono::milliseconds>(t2_internal - t1_internal).count();
            if (local_result != ULLONG_MAX) {
                result = local_result;
                if (duration_internal > 0) {
                    hash_rate = (local_result - s_index) / duration_internal;
                } else {
                    hash_rate = INT_MAX;
                }
                cout << "Block: " << block_iter << " Hash Rate: " << hash_rate << endl;
            }
            MPI_Barrier(MPI_COMM_WORLD);
            auto t2 = chrono::high_resolution_clock::now();
            auto duration = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();
            if (world_rank == 0) {
                cout << " Execution Time (milliseconds): " << duration << " Nonce: "
                     << result << " Leading zeros: " << compute_leading_zeros(target_hash) << " Target Hash: "
                     << target_hash << endl;

            }
            // Find the next target hash
            target_hash = compute_target_hash(target_hash, block_iter, duration);
        }

        auto t2_overall = chrono::high_resolution_clock::now();
        auto duration_overall = chrono::duration_cast<chrono::milliseconds>(t2_overall - t1_overall).count();
        if (world_rank == 0)
            cout << "Total time (milliseconds):: " << duration_overall << endl;
        MPI_Win_free(&win);
        MPI_Finalize();
    } else {
        if(nDevices > 0) {
            cout << "Thread " << world_rank << " is going to use " << world_rank%nDevices << " / " << nDevices << " GPUs" << endl;
	    // main_kernel(world_rank%nDevices, nDevices);
        }
	MPI_Finalize();
    }
    return 0;
}

